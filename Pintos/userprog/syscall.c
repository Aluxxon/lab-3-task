#include <stdio.h>
#include "userprog/syscall.h"
#include "userprog/process.h"
#include "userprog/pagedir.h"
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "threads/malloc.h"
#include "filesys/filesys.h"
#include "filesys/file.h"
#include "devices/input.h"
#include "devices/shutdown.h"

struct file_descriptor
{
	struct file *file_struct;
	struct list_elem elem;
};	
				  
static uint32_t *esp;

static void syscall_handler (struct intr_frame *);
static bool is_valid_uvaddr (const void *);
static struct file_descriptor *get_open_file (int);	
static int allocate_fd (void);	   
static void close_open_file (int);
static int allocate_fd (void);

/******** FUNCTIONS FOR SYSTEM CALLS *********/

static void halt(void);
static bool create(const char*, unsigned);
static pid_t exec(const char *);
static int wait(pid_t);
static void seek(int, unsigned);
static int filesize(int);
static int open(const char *);
static int read(int, void *, unsigned);
static int write(int, const void *, unsigned);
static void exit(int);
static bool remove(const char *);
static unsigned tell(int);
static void close(int);

/***** END OF FUNCTIONS FOR SYSTEM CALLS *****/

void syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
  lock_init (&fs_lock);
  list_init (&open_files); 
}

static void syscall_handler (struct intr_frame *f) {
	if (!is_valid_ptr (esp) || !is_valid_ptr (esp + 1) || !is_valid_ptr (esp + 2)
		|| !is_valid_ptr (esp + 3))
    {
		exit(-1);
    }
	
	else
    {
		int syscall_number = *esp;
		
		if (syscall_number == SYS_HALT)
        {
            halt();
        }

        else if (syscall_number == SYS_CREATE)
        {
            f->eax = create((char *) *(esp + 1), *(esp + 2));
        }

        else if (syscall_number == SYS_EXEC)
        {
            f->eax = exec((char *) *(esp + 1));
        }

        else if (syscall_number == SYS_WAIT)
        {
            f->eax = wait(*(esp + 1));
        }

        else if (syscall_number == SYS_SEEK)
        {
            seek(*(esp + 1), *(esp + 2));
        }

        else if (syscall_number == SYS_FILESIZE)
        {
	        f->eax = filesize(*(esp + 1));
        }

        else if (syscall_number == SYS_OPEN)
        {
            f->eax = open((char *) *(esp + 1));
        }

        else if (syscall_number == SYS_READ)
        {
            f->eax = read(*(esp + 1), (void *) *(esp + 2), *(esp + 3));
        }

        else if (syscall_number == SYS_WRITE)
        {
            f->eax = write(*(esp + 1), (void *) *(esp + 2), *(esp + 3));
        }

        else if (syscall_number == SYS_EXIT) 
        {
          exit(*(esp + 1));
        }

        else if (syscall_number == SYS_REMOVE)
        {
            f->eax = remove((char *) *(esp + 1));
        }

        else if (syscall_number == SYS_TELL)
        {
            f->eax = tell(*(esp + 1));
        }

        else if (syscall_number == SYS_CLOSE)
        {
            close(*(esp + 1));
        }

        else 
        {
            break;
        }
	}
	
	
/************************************ ALL SYSTEM CALL FUNCTIONS ************************************/



/*****************************************************************
Function: hault()
Description: Stops the operating system.
Parameters: void
Returns: None
*****************************************************************/

void halt(void)
{
    shutdown_power_off();
}


/*****************************************************************
Function: create()
Description: Creates a file.
Parameters: char *file, unsigned initial_size
Return: current_position
*****************************************************************/

bool create(const char *file, unsigned initial_size)
{
    bool current_position;

    if (!is_valid_ptr (file))
        exit (-1);

    lock_acquire(&fs_lock);

    status = filesys_create(file, initial_size); 

    lock_release(&fs_lock);

    return current_position;
}
 

/*****************************************************************
Function: pid_t exec()
Parameters: char *cmd_line
Return: tid
*****************************************************************/

pid_t exec(const char *cmd_line)
{
    /*
    * Stores the thread ID, which will be assigned to pid
    * when there is a user process occuring within a kernel thread
    */
    tid_t tid;
    struct thread *current_thread;

    //validate the user pointer
    if (!is_valid_ptr(cmd_line))
    {
        exit(-1);
    }

    current_thread = thread_current();
    current_thread->child_load_status = 0;

    tid = process_execute(cmd_line);

    lock_acquire(&cur->lock_child);

    while (current_thread->child_load_status == 0)
        cond_wait(&cur->cond_child, &current_thread->lock_child);

    if (current_thread->child_load_status == -1)
        tid = -1;

    lock_release(&current_thread->lock_child);

    return tid;
}


/*****************************************************************
Function: wait()
Description: W
Parameters: pid_t pid
Returns: process_wait(pid)
/****************************************************************/

int wait(pid_t pid)
{ 
  return process_wait(pid);
}



/*****************************************************************
Function: seek()
Description: 
Parameters: int fd, unsigned position
Returns: None
/****************************************************************/

void seek(int fd, unsigned position)
{
    struct file_descriptor *file_descriptor_struct;

    lock_acquire(&fs_lock); 

    file_descriptor_struct get_open_file(fd);

    if (file_descriptor_struct != NULL)
        file_seek(file_descriptor_struct->file_struct, position);

    lock_release(&fs_lock);
    
    return;
}


/*****************************************************************
Function: filesize()
Description: Get the size of a file.
Parameters: int fd
Return: current_position
*****************************************************************/

int filesize(int fd)
{
    struct file_descriptor *file_descriptor_struct;
    int current_position = -1;

    lock_acquire(&fs_lock);

    file_descriptor_struct = get_open_file (fd);

    if (file_descriptor_struct != NULL)
        current_position = file_length (file_descriptor_struct->file_struct);

    lock_release(&fs_lock);

    return current_position;
}


/*****************************************************************
Function: open()
Description: Opens a file.
Parameters: char *file
Return: current_position
*****************************************************************/

int open(const char *file)
{
    struct file *fileNode;
    struct file_descriptor *file_description;
    int current_position = -1;
  
    if (!is_valid_ptr (file))
        exit(-1);

    lock_acquire (&fs_lock); 
 
  fileNode = filesys_open(file);
    if (fileNode != NULL)
    {
        file_description = calloc(1, sizeof *fd);
        file_description->fd_num = allocate_fd();
        file_description->owner = thread_current()->tid;
        file_description->file_struct = fileNode;

        list_push_back(&open_files, &file_description->elem);

        current_position = file_description->fd_num;
    }

    lock_release(&fs_lock);

    return current_position;
}


/*****************************************************************
Function: read()
Description: Reads content into buffer.
Parameters: int fd, void buffer, unsigned size
Return: current_position 
*****************************************************************/

int read(int fd, void *buffer, unsigned size)
{
    struct file_descriptor *file_descriptor_struct;
    int current_position = 0;

    struct thread *current_thread = thread_current();

    unsigned size_of_buffer = size;
    void * buffer_temporary = buffer;

  //check the memory pointed by the buffer is null
    while (buffer_temporary != NULL)
    {
        if (!is_valid_uvaddr(buffer_temporary))
        exit (-1);

        if (pagedir_get_page(t->pagedir, buffer_temporary) == NULL)   
        { 
            struct suppl_pte *spte;

            spte = get_suppl_pte(&t->suppl_page_table, pg_round_down(buffer_temporary));

        if (spte != NULL && !spte->is_loaded)
            load_page(spte);

        else if (spte == NULL && buffer_temporary >= (esp - 32))
            grow_stack(buffer_temporary);

        else
            exit(-1);
        }
        
        //go forwards
        if (size_of_buffer == 0)
        {
            buffer_temporary = NULL;
        }

        //stop
        else if (size_of_buffer > PGSIZE)
        {
            buffer_temporary = buffer_temporary + PGSIZE;
            size_of_buffer = size_of_buffer - PGSIZE;
        }
        
        else
        {
            buffer_temporary = buffer + size - 1;
            size_of_buffer = 0;
        }
    }

    lock_acquire(&fs_lock);  

    if (fd == STDOUT_FILENO)
        current_position = -1;
        
    else if (fd == STDIN_FILENO)
    {
        uint8_t c;
        unsigned token = size;
        uint8_t *buffer_node = buffer;

        while (token > 1 && (c = input_getc()) != 0)
            {
                *buffer_node = c;
                buffer++;
                token--; 
            }

        *buffer_node = 0;
        current_position = size - token;
    }

    else 
    {
        file_descriptor_struct = get_open_file(fd);

        if (file_descriptor_struct != NULL)
            current_position = file_read(file_descriptor_struct->file_struct, buffer, size);
    }

    lock_release(&fs_lock);

    return current_position;
}


/*****************************************************************
Function: write()
Description: Writes content from a buffer into the file.
Parameters: int fd, void buffer, unsigned size
Return: current_position 
*****************************************************************/

int write(int fd, const void *buffer, unsigned size)
{
    struct file_descriptor *file_descriptor_struct;  
    int current_position = 0;

    unsigned size_of_buffer = size;
    void *buffer_temporary = buffer;

    //check the memory pointed by the buffer is null
    while (buffer_temporary != NULL)
    {
        if (!is_valid_ptr(buffer_temporary))
	        exit (-1);
      
        //go forwards
        if (size_of_buffer > PGSIZE)
        {
            buffer_temporary =  buffer_temporary + PGSIZE;
            size_of_buffer = size_of_buffer - PGSIZE;
	    }

        else if (size_of_buffer == 0)
	    {
            //stop
            buffer_temporary = NULL;
	    }
        
        else
        {
            buffer_temporary = buffer + size - 1;
            size_of_buffer = 0;
        }
    }

    lock_acquire(&fs_lock); 

    if (fd == STDIN_FILENO)
    {
        current_position = -1;
    }

    else if (fd == STDOUT_FILENO)
    {
        putbuf(buffer, size);;
        current_position = size;
    }

    else 
    {
        file_descriptor_struct = get_open_file(fd);

        if (file_descriptor_struct != NULL)
            current_position = file_write(file_descriptor_struct->file_struct, buffer, size);
    }
    
    lock_release(&fs_lock);

    return current_position;
}


/*****************************************************************
Function: exit()
Description: Stops the thread that is running currently.
Parameters: int status
Returns: None
*****************************************************************/

void exit(int status)
{
    struct child_status *offspring;
    struct thread *current_thread = thread_current();

    //print exit 
    printf ("%s: exit(%d)\n", current_thread->name, status);

    struct thread *parentThread = thread_get_by_id(current_thread->parent_id);

    if (parentThread!= NULL) 
    {
      struct list_elem *e = list_tail(&parentThread->children);
      while ((e = list_prev (e)) != list_head(&parentThread->children))
        {
          child = list_entry(e, struct child_status, elem_child_status);

          if (offspring->child_id == current_thread->tid)
          {
            lock_acquire(&parentThread->lock_child);

            offspring->is_exit_called = true;
            offspring->child_exit_status = status;

            lock_release(&parentThread->lock_child);
          }
        }
    }
    thread_exit();
}


/*****************************************************************
Function: remove() 
Description: Removes a file.
Parameters: char *file
Return: current_position
*****************************************************************/

bool remove(const char *file)
{
    bool current_position;

    if (!is_valid_ptr(file))
    exit (-1);

    lock_acquire (&fs_lock);  

    status = filesys_remove (file);

    lock_release (&fs_lock);

    return current_position;
}


/*****************************************************************
Function: tell()
Description: Get the current position of a file.
Parameters: int fd
Returns: position 
*****************************************************************/

unsigned tell(int fd)
{
    int position = 0;
    struct file_descriptor *file_descriptor_struct;

    lock_acquire(&fs_lock); 
    file_descriptor_struct = get_open_file(fd);

    if (fd_struct != NULL)
        position = file_tell file_descriptor_struct->file_struct);

    lock_release(&fs_lock);

    return position 
}


/*****************************************************************
Function: close()
Description: Closes a file.
Parameters: int fd
Returns: None
*****************************************************************/

void close(int fd)
{
    struct file_descriptor *file_descriptor_struct;

    lock_acquire(&fs_lock); 
    
    file_descriptor_struct = get_open_file(fd);

    if (*file_descriptor_struct != NULL && file_descriptor_struct->owner == thread_current()->tid)
        close_open_file(fd);

    lock_release(&fs_lock);

    return ; 
}


/************************************ END OF ALL SYSTEM CALL FUNCTIONS ************************************/											   
