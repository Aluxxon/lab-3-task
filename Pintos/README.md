# GROUP MEMBERS
- Ismail Alson Ali Rasheed (S2000811)
- Ibrahim Hassan Didi (S2000410)

# CONTRIBUTIONS TO THIS PROJECT
- Ismail Alson Ali Rasheed (S2000811)
- Ibrahim Hassan Didi (S2000410)

# SUMMARY
This is a variant of the Pintos Operating System for use as part of UFCFWK-15-2 Operating Systems module. 2019/20
This project folder contains implementation Argument passing and a total of 13 system 
- This is a group work which asks for the implementation of Argument Passing and over 13 System Calls

# FILES THAT HAVE BEEN CHANGED IN THIS FOLDER
- thread.h (Location: Pintos/threads)
- process.h (Location: Pintos/userprog)
- process.c (Location: Pintos/userprog)
- syscall.c (Location: Pintos/userprog)

